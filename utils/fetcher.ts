import { IUsersData } from "../interfaces"

export const fetcherAsync = async <T>(uri: string): Promise<(IUsersData & T)> => {
  try {
    const response = await fetch(uri)
    const responseJSON = await response.json()
  
    if (response.status !== 200) {
      throw new Error("error request")
    }

    return responseJSON
  } catch (error) {
    const message = error.message

    console.log(message, 'fetch error')
  }
}
