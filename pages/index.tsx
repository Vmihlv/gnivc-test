import React from "react";
import { observer } from "mobx-react-lite";
import { UserDataList } from "../components/UserDataList";

const IndexPage = observer(() => <UserDataList />);

export default IndexPage;
