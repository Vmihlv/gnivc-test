import React from "react";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { UserStoreProvider } from "../providers/userStoreProvider";

import "@fontsource/roboto";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: "Roboto";
    background: rgb(199,199,199);
  }
`;

const theme = {
  colors: {
    primary: "#0070f3",
  },
};

export default function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />

      <ThemeProvider theme={theme}>
        <UserStoreProvider>
          <Component {...pageProps} />
        </UserStoreProvider>
      </ThemeProvider>
    </>
  );
}