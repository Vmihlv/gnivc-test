import React from "react";
import styled from "styled-components";
import { Card, CardContent, Tooltip, Typography } from "@material-ui/core";
import { StatusDot } from "./StatusDot";
import { observer } from "mobx-react-lite";
import { IUser } from "../interfaces";
import { UserStore } from "../store/UserStore";
import { useRootStore } from "../providers/userStoreProvider";

type Props = {
  userStore?: UserStore;
  data: IUser;
};

const StyledCard = styled(Card)`
  margin: 20px 0;
  align-items: center;
  justify-content: center;
`;

const StyledHoverText = styled(Typography)`
  color: wheat;

  &:hover {
    color: black;
    transition-duration: 0.5s;
  }
`;

const StyledCardContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  margin: auto;
  max-width: max-content;
  text-align: center;
`;

export const UserDataCard = observer((props: Props) => {
  const userStore = useRootStore();
  const isOnline = props.data.status === "active";

  return (
    <StyledCardContainer key={props.data.id}>
      <StyledCard>
        <CardContent>
          <Typography>
            {props.data.name}

            <StatusDot isOnline={isOnline} />
          </Typography>

          <Typography color="textSecondary" gutterBottom>
            {props.data.gender}
          </Typography>

          <Tooltip title={`Posts count - ${userStore.postsCounter}`}>
            <StyledHoverText
              variant="body2"
              component="a"
              onMouseEnter={() => userStore.getPostsCount(props.data.id)}
            >
              {props.data.email}
            </StyledHoverText>
          </Tooltip>
        </CardContent>
      </StyledCard>
    </StyledCardContainer>
  );
});
