import { Typography } from '@material-ui/core'

export const Loader = () => <Typography variant="h5"> Loading...</Typography>
