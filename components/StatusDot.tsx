import styled from "styled-components";
import { dot } from "../utils/variables";

const StyledStatusDot = styled.span`
  color: ${(props) => props.isOnline ? "green" : "red"};
  padding: 0 5px;
`;

export const StatusDot = ({isOnline}) => <StyledStatusDot isOnline={isOnline} children={dot}/>
