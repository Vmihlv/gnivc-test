import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { Loader } from "./Loader";
import { EndMessage } from "./EndMessage";
import { UserDataCard } from "./UserDataCard";
import { observer } from "mobx-react-lite";
import { useRootStore } from "../providers/userStoreProvider";

export const UserDataList = observer(() => {
  const userStore = useRootStore();
  const hasUsers = userStore.users?.length > 0;

  if (!hasUsers) {
    return null;
  }

  return (
    <InfiniteScroll
      dataLength={userStore.users.length}
      next={userStore.getMoreUsers}
      hasMore={!userStore.isEndOfList}
      loader={<Loader />}
      endMessage={<EndMessage />}
    >
      {userStore.users.map((data) => (
        <UserDataCard key={data.id} data={data} />
      ))}
    </InfiniteScroll>
  );
});
