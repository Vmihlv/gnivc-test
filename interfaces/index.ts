export interface IUser {
  email: string
  name: string
  id: number
  gender: IUserGender
  status: IUserStatus
}

export type IUsersData = {
  code: number
  meta: IUserPostsMeta
  data: IUser[]
}

export type IPostData = {
  code: number
  meta: IUserPostsMeta
  data: IUserPostsData[]
}

export type IUserPostsMeta = {
  pagination: {
    total: number
    pages: number
    page: number
    limit: number
  }
}

export type IUserPostsData = {
  [key: number] : {
    id: number
    user_id: number
    title: string
    body: string
  }
}

export type IUserStatus = "active" | "inactive"

export type IUserGender = "male" | "female"
