import { enableStaticRendering } from "mobx-react-lite";
import { createContext, ReactNode, useContext } from "react";
import { UserStore } from "../store/UserStore";

const isServer = typeof window === "undefined"

enableStaticRendering(isServer);

let store: UserStore;
const StoreContext = createContext<UserStore | undefined>(undefined);

export function useRootStore() {
  const context = useContext(StoreContext);
  if (context === undefined) {
    throw new Error("useRootStore must be used within RootStoreProvider");
  }

  return context;
}

export function UserStoreProvider({ children }: { children: ReactNode }) {
  const store = initializeStore();

  return (
    <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
  );
}

function initializeStore(): UserStore {
  const _store = store ?? new UserStore();
  _store.getMoreUsers();

  if (isServer) return _store;
  if (!store) store = _store;

  return _store;
}
