import { makeAutoObservable, runInAction } from "mobx";
import { IUser } from "../interfaces";
import { fetcherAsync } from "../utils/fetcher";

export class UserStore {
  pageCounter = 1;
  postsCounter = 0;
  isEndOfList = false;
  users: IUser[];

  constructor() {
    makeAutoObservable(this);
    this.users = []
  }

  fetchUsersList = async (pageCounter: number) => {
    const res = await fetcherAsync(
      `https://gorest.co.in/public-api/users?page=${pageCounter}`
    );

    this.isEndOfList = res.meta.pagination.pages <= pageCounter;

    return res;
  };

  fetchUserPosts = async (id: number) => {
    const res = await fetcherAsync(
      `https://gorest.co.in/public-api/posts?user_id=${id}`
    );

    return res;
  };

  getMoreUsers = async () => {
    const newUsers = await this.fetchUsersList(this.pageCounter);

    runInAction(() => {
      this.users = [...this.users, ...newUsers.data];
      this.pageCounter++;
    });
  };

  getPostsCount = async (id: number) => {
    const postsCount = await this.fetchUserPosts(id);

    runInAction(() => {
      this.postsCounter = postsCount.data.length
    });
  };
}
